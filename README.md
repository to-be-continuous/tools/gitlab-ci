# GitLab CI Stuff

## Overview

This template provides a jobs to validate your template syntax.

It uses the [GitLab CI Lint API](https://docs.gitlab.com/ee/api/lint.html).

## Template validation

The template implements a `gitlab-ci-lint` job, enabled with `GITLAB_CI_FILES` variable (GitLab CI files pattern(s) to analyse).

Example of a `.gitlab-ci.yml` file for a GitLab CI template project:

```yaml
include:
  - project: 'to-be-continuous/tools/gitlab-ci'
    ref: 'master'
    file: '/templates/validation.yml'

stages:
  - build

variables:
  GITLAB_CI_FILES: "templates/*.yml"
```
## Variants

The default validation template is designed to work on untagged runners, without any proxy configuration using Docker images
from the internet and using Default Trusted Certificate Authorities.

Nevertheless there are template variants available to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url          | **must be defined** |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | _none_ |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | _none_ |

By default, the variant will authentifacte using a [JWT ID token](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html). To use [AppRole](https://www.vaultproject.io/docs/auth/approle) instead the `VAULT_ROLE_ID` and `VAULT_SECRET_ID` should be defined as secret project variables.

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: $CI_SERVER_FQDN/to-be-continuous/tools/gitlab-ci/validation@master
  # Vault variant
  - component: $CI_SERVER_FQDN/to-be-continuous/tools/gitlab-ci/validation-vault@master
    inputs:
      # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"

variables:
  # Secrets managed by Vault
  GITLAB_TOKEN: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-infra/gitlab?field=token"
```

